import unittest
from Game.Validators.validator import Validator


class TestValidator(unittest.TestCase):
    def setUp(self):
        self.validator = Validator()

    def test_word_verification(self):
        real_words = ["obscure", "hilarious", "epiphany", "ubiquitous", "ledge"]
        for word in real_words:
            self.assertTrue(self.validator.validate_word(word))

        false_words = ["opiphany", "obscur", "hilarias", "lege"]
        for word in false_words:
            self.assertFalse(self.validator.validate_word(word))

    def test_index_validation(self):
        incorrect_indexes = ["G:16", "Z:9", "a:7"]
        for index in incorrect_indexes:
            self.assertFalse(self.validator.validate_index(index))

        correct_indexes = ["G:8", "A:4", "N:14", "J:1"]
        for index in correct_indexes:
            self.assertTrue(self.validator.validate_index(index))


if __name__ == '__main__':
    unittest.main()
