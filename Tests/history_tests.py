import unittest
from Game.Model.history import History


class HistoryTests(unittest.TestCase):
    def setUp(self):
        self.history = History()
        self.history.document_turn(1, "Charlie Sheen", "play", "winning", 100)

    def test_document_turn(self):
        record = (1, "Charlie Sheen", "play", "winning", 100)
        self.assertIn(record, self.history.words)

    def test_is_duplicate(self):
        self.assertTrue("winning")  # word already played by "Charlie Sheen" in setUp()


if __name__ == '__main__':
    unittest.main()
