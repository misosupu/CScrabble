import unittest
from Game.Model.tileholder import TileHolder
from Game.Model.bag import Bag


class TestTileHolder(unittest.TestCase):
    def setUp(self):
        self.bag = Bag()
        self.rack = TileHolder(self.bag)

    def test_tile_holder(self):
        self.assertEqual(len(self.rack.tiles), 7, "Test failed on initializing a new tile holder.")

    def test_draw(self):
        self.assertFalse(self.rack.draw_letters(2, self.bag))

    def test_remove_letters(self):
        self.rack.empty()
        self.rack = [('a', 1), ('b', 3), ('s', 1), ('z', 10),
                     ('h', 4), ('a', 1), ('c', 3)
                     ]

        self.rack.remove_letters([('h', 4), ('s', 1), ('a', 1)], self.bag)

        # check if letter 'h' is in rack
        self.assertEqual(len([True for elem in self.rack if elem[0] == 'h']) > 0,
                         False)
        # check if letter 's' is in rack
        self.assertEqual(len([True for elem in self.rack if elem[0] == 's']) > 0,
                         False)

        # check if letter 'a' is in rack
        self.assertEqual(len([True for elem in self.rack if elem[0] == 'a']) > 0,
                         True)


if __name__ == '__main__':
    unittest.main()
