import unittest
from Game.scrabble import Board
from Game.scrabble import BoardInspector

class TestInspector(unittest.TestCase):
    def setUp(self):
        self.board = Board()
        self.inspector = BoardInspector()
        self.board.place_tiles(('G', '8'), ('I', '8'), ['t', 'o', 'e'])
        result = self.inspector.update_options(self.board)
        self.board.place_tiles(('G', '6'), ('G', '8'), ['r', 'a', 't'])
        result = self.inspector.update_options(self.board)
        self.board.place_tiles(('I', '7'), ('I', '9'), ['p', 'e', 't'])

    def test_horizontal_options(self):
        free = [(5, 5), (5, 4), (5, 3), (5, 2), (5, 1),
                (5, 0), (5, 7), (5, 8), (5, 9), (5, 10),
                (5, 11), (5, 12), (6, 5), (6, 4), (6, 3),
                (6, 2), (6, 1), (6, 0), (6, 7), (7, 5),
                (7, 4), (7, 3), (7, 2), (7, 1), (7, 0),
                (6, 7), (6, 9), (6, 10), (6, 11), (6, 12),
                (6, 13), (6, 14), (7, 9), (7, 10), (7, 11),
                (7, 12), (7, 13), (7, 14), (8, 7), (8, 6),
                (8, 5), (8, 4), (8, 3), (8, 2), (8, 9),
                (8, 10), (8, 11), (8, 12), (8, 13), (8, 14)
                ]
        options = set(free)
        # result consists of list of lists - has to be flattened
        result = [item for sub in result for item in sub]
        self.assertEqual(options, set(result))

    def test_vertical_options(self):
        pass
