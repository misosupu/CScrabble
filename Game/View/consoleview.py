class ConsoleView(object):
    """
    View class corresponding to the MVC design pattern paradigm.
    Draws on screen what the game model (in particular the "Board" class) represents.
    """

    @staticmethod
    def show_board(game_board):
        """ Handles printing the current board information to the console. """
        def stringify(elem):
            if len(elem) >= 3:
                raise UnboundLocalError
            return str(elem) + ' ' * (3 - len(elem))

        def print_row(n, row):
            n_indent = "" if n >= 9 else " "
            print(str(n + 1) + n_indent, "  ".join(stringify(elem) for elem in row))
        print('   ', end='')
        print("    ".join("ABCDEFGHIJKLMNO"))
        for number, row in enumerate(game_board.grid):
            print_row(number, row)

    @staticmethod
    def rack_info(player):
        """
        :param player: object of type Player
        :return: current tile state in "Player"'s rack.
        """
        print(player.tiles)

    @staticmethod
    def list_actions():
        """ Action notifier for each player turn. """
        print("What would you like to do?")
        print("1. play word\n2. change letters\n3. pass\n4. resign\n5. get word definition")

    @staticmethod
    def invalid_initial_word():
        """ Notifier for an invalid initial word.
            The Scrabble® rules require the first word to intersect with cell (8, 8). """
        print("First word cells should intersect with cell (8, 8).\n"
              "Enter valid coordinates for start and end.")

    @staticmethod
    def turn_info(player_name, word, word_points):
        """ Notifier for a player's turn with information about played word and corresponding points. """
        print("Player \"{}\" played the word \"{}\" for {} points.".format(player_name, word, word_points))

    @staticmethod
    def player_passive(player_name, action):
        """ Notifies when a player has chosen to pass their turn. """
        print("Player \"{}\" has {}.".format(player_name, action))

    @staticmethod
    def player_changed_tiles(player_name):
        """ Notifies when a player has chosen to change tiles as their turn. """
        print("Player \"{}\" has made a move - change tiles.".format(player_name))

    @staticmethod
    def announce_winner(winner):
        """ Announces game winner and their corresponding points. """
        print("The winner is player \"{}\" with {} points".format(winner.name, winner.points))

    @staticmethod
    def announce_turn(player):
        """ Announces which player's turn it is. """
        print("Player \"{}\"'s turn.".format(player.name))

    @staticmethod
    def announce_invalid_word_played(word, player_name):
        """ Announces player has entered an invalid word and
            therefore is suspended from current turn. """
        print("The word \"{}\" player \"{}\" has entered is incorrect. Invalid move.".format(word, player_name))

    @staticmethod
    def display_error_message(error_type):
        """ Template for user input error message.
            Variable "error_type" describes the error. """
        print("Invalid {} input. Try again".format(error_type))

    @staticmethod
    def announce_maximum_players_exceeded():
        """ Announce that the maximum number of players for a game
           (which, according to the Scrabble® rules) is 4, is exceeded. """
        print("The maximum number of players is achieved.\nCannot add any more.")
