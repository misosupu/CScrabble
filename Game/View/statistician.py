import matplotlib.pyplot as plt


class Statistician(object):
    @staticmethod
    def output_statistics(game_history, players):
        players_data = [[]] * len(players)
        for player in players:
            points = [element[2] for element in game_history if element[4] == player]
            players_data.append([player, points])

        for i in range(len(players)):
            plt.plot(players_data[i][1], 3)

        plt.xlabel('Time')
        plt.ylabel('Scores')
        plt.title('Statistics for recent game.')
        plt.show()
