class History(object):
    def __init__(self):
        """
        Holds list of words containing tuples of the sort:
        (action_type, word, points, turn, player_name).
        """
        self.words = []

    def document_turn(self, turn, player_name, action, word, score):
        self.words.append((turn, player_name, action, word, score))

    def is_duplicate(self, word):
        for record in self.words:
            if word == record[3]:
                return record
        return False
