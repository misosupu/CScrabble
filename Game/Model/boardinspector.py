class BoardInspector(object):  # TODO: add another turn option "Help" - connects player with board inspector
    """
    Inspects the board state after every move and keeps track
    of the places, where a move is theoretically possible to
    be made. Returns a sorted (by priority) list of possible "word slots".
    """

    def __init__(self):
        self.rows = {}  # holds rows where a move is possible to be made
        self.columns = {}  # analogously - holds columns, where a move is possible to be made

    def __str__(self):
        return "Rows: {}\nColumns: {}".format(self.rows, self.columns)

    @staticmethod
    def get_row(board, r):
        """ Get list of the indexes of the free cells on row "r". """
        # width = len(board.grid)
        width = len(board)
        row = []
        for col in range(width):
            if board.grid[r][col].is_empty is True:
                row.append((r, col))
            else:
                row.append((r, col, board.grid[r][col].holds_letter))
        return row

    @staticmethod
    def get_column(board, col):
        """ Get list of the indexes of the free cells on column "col". """
        # height = len(board.grid)
        height = len(board)
        column = []
        for row in range(height):
            if board.grid[row][col].is_empty is True:
                column.append((row, col))
            else:
                column.append((row, col, board.grid[row][col].holds_letter))
        return column

    def update_options_3(self, board, coords):
        """
        Update possible options after a new word has been played on board.
        :param coords: holds the coordinates of the last played word on board.
        """

        if coords[0][0] == coords[1][0]:
            word_type = "h"  # eg. horizontal
        else:
            word_type = "v"  # eg. vertical

        if word_type == 'h':
            i = coords[0][0]  # the word is vertical so the changing part is the coordinate corresponding to the column
            j_first = coords[0][1]  # the first column index
            j_last = coords[-1][1]  # the last column index
            for k in range(-1, 2):
                if (i + k) in range(0, 15):
                    self.rows[i + k] = BoardInspector.get_row(board, i + k)
            if j_first - 1 in range(0, 15):
                self.columns[j_first - 1] = BoardInspector.get_column(board, j_first - 1)
            if j_last + 1 in range(0, 15):
                self.columns[j_last + 1] = BoardInspector.get_column(board, j_last + 1)
            for coord in coords:
                self.columns[coord[1]] = BoardInspector.get_column(board, coord[1])
        else:
            j = coords[0][1]  # the word is horizontal so the changing part is the coordinate, corresponding to the row
            i_first = coords[0][0]  # the first row index
            i_last = coords[-1][0]  # the last row index
            for k in range(-1, 2):
                if (j + k) in range(0, len(board)):
                    self.columns[j + k] = BoardInspector.get_column(board, j + k)
            if i_first - 1 in range(0, len(board)):
                self.rows[i_first - 1] = BoardInspector.get_row(board, i_first - 1)
            if i_last - 1 in range(0, len(board)):
                self.rows[i_last + 1] = BoardInspector.get_row(board, i_last + 1)
            for coord in coords:
                self.rows[coord[0]] = BoardInspector.get_row(board, coord[0])

    @staticmethod
    def __get_priority_score(l, board):
        """
        Calculate priorities of each element, based on a priority heuristic.
        The priorities are: TW: 4, DW: 3, TL: 2, DL: 1.
        :param l: in the form [(i1, j1[, letter]), (i2, j2[, letter]), ... ] - indexes for said group
        :param board: current game board
        """

        priority = 0
        for elem in l:
            i, j = elem[0], elem[1]  # elem[2] equals a letter, if tile is already taken
            if board.grid[i][j].is_empty:
                if board.grid[i][j].type == 'TW':
                    priority += 4
                elif board.grid[i][j].type == 'DW':
                    priority += 3
                elif board.grid[i][j].type == 'TL':
                    priority += 2
                elif board.grid[i][j].type == 'DL':
                    priority += 1
        return priority

    def prioritize_options(self, board):
        # def get_taken_cells(l):
        #     indexes = [i for i in range(len(l)) if len(l[i]) == 3]  # ie - if l[i] is in the form (i, j, letter)
        #     sublists = []
        #     for index in indexes:
        #         sublists.append(l[index:(index + 7)])

        priorities = []
        options = list(self.rows.values())
        options.extend(self.columns.values())
        for l in options:
            score = BoardInspector.__get_priority_score(l, board)
            priorities.append((score, l))

        # sort priorities based on keys by first value (e.g. - "score"):
        priorities.sort(key=lambda x: x[0], reverse=True)
        return priorities

    @staticmethod
    def test_slot(slot, board, permutations, validator):
        """ slot is in the form [(i1, j1 [, letter1]), (i2, j2 [, letter2]), ...]. """
        import time
        start_time = time.clock()  # keep track of how much time has elapsed to avoid slowing down the game

        def get_slot_layout(slot):
            if slot[0][0] == slot[1][0]:  # if two consecutive "i" coordinates are the same => the word is horizontal
                return 'h'
            else:  # two consecutive "i" coordinates differ => the word is vertical
                return 'v'

        def get_filled_indexes(l):
            """ if the length of an element in l is 3, then it's in the form (ik, jk, letterk) """
            return [i for i in range(len(l)) if len(l[i]) == 3]

        def get_n_length_subsets(slot, length):
            filled_indexes = get_filled_indexes(slot)  # ie - if l[i] is in the form (i, j, letter)
            subsets = []
            for i in range(0, len(slot) - length + 1):
                filled = 0
                unfilled = 0
                for j in range(i, len(slot)):
                    if j in filled_indexes:
                        filled += 1
                    else:
                        unfilled += 1
                        if unfilled == length:
                            break
                if filled != 0:
                    if i + length + filled <= len(slot):
                        if len(slot[i - 1]) < 3 and len(slot[i + length + filled - 1]) < 3:
                            subsets.append([slot[i] for i in range(i, i + length + filled)])
                    else:
                        break
            return subsets
        while time.clock() - start_time < 30:
            for word_len in range(7, 1, -1):
                # while flag is not True:  # eg - while a valid word has not been found
                    subsets = get_n_length_subsets(slot, word_len)
                    for permutation in permutations[word_len]:
                        for subset in subsets:
                            temp = list(permutation)  # because itertools.permutation returns tuples
                            filled_indexes = get_filled_indexes(subset)
                            for index in filled_indexes:
                                temp.insert(index, subset[index][2])
                            word = ''.join(temp)
                            if validator.validate_word(word):
                                # print("Word combination found: {}".format(word))
                                partial_coordinates = [element for element in subset if len(element) != 3]
                                # check if the intersections are real words as well:
                                intersections = board.get_intersecting_words(get_slot_layout(subset), subset,
                                                                             partial_coordinates, word)
                                if validator.validate_intersections(intersections) is True:
                                    # print("I can play the word \"{}\"". format(word))
                                    return word, subset, filled_indexes, intersections

        return None
