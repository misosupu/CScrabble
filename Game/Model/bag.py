import random


class Bag(object):
    """
    Contains all the letter tiles and information about each ones
    score and current quantity.
    """
    def __init__(self):
        self.bag = {
            'a': [1, 9], 'b': [3, 2], 'c': [3, 2],
            'd': [2, 4], 'e': [1, 12], 'f': [4, 2],
            'g': [2, 3], 'h': [4, 2], 'i': [1, 9],
            'j': [8, 1], 'k': [5, 1], 'l': [1, 4],
            'm': [3, 2], 'n': [1, 6], 'o': [1, 8],
            'p': [3, 2], 'q': [10, 1], 'r': [1, 6],
            's': [1, 4], 't': [1, 6], 'u': [1, 4],
            'v': [4, 2], 'w': [4, 2], 'x': [8, 1],
            'y': [4, 2], 'z': [10, 1]
        }

    @property
    def available_tiles(self):
        """ Keeps track of the number of tiles currently in the bag. """
        return sum([quantity for (score, quantity) in self.bag.values()])

    def get_letter_score(self, letter):
        """ Returns the score of "letter" from bag. """
        return self.bag[letter][0]

    def draw_tiles(self, number_of_tiles):
        """ Draws selected number of tiles from bag and updates its current contents (quantity-wise). """
        available_tiles = [y for key in self.bag.keys() for y in [key] * self.bag[key][1] if self.bag[key][1] != 0]
        l = random.sample(available_tiles, number_of_tiles)
        for elem in l:
            self.bag[elem][1] -= 1
        return [(letter, self.bag[letter][0]) for letter in l]

    def return_tiles(self, to_replace):
        """ Returns the tiles in "to_replace" to the bag and updates its contents (quantity-wise). """
        # TODO: check if to_replace is a subset of players tiles
        for elem in to_replace:
            value = self.bag.get(elem)
            value[1] += 1
            self.bag[elem] = [value[0], value[1]]

    def is_empty(self):
        """ Check whether all the quantities of the letters in the bag are equal to 0. """
        return len({key for key in self.bag if self.bag[key][1] != 0}) == 0

    # def current_contents(self):
    #     for elem in self.bag:
    #         return self.bag[elem]
