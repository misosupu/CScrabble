class TileHolder(object):
    def __init__(self, bag):
        self.tiles = bag.draw_tiles(7)

    def __str__(self):
        return "The holder has the following letters {}".format(self.tiles)

    def add(self, letters):
        self.tiles.extend(letters)

    def remove_letters(self, letters_with_points, bag):
        # TODO: function trying to remove letters from previous turn in current word
        for letter in letters_with_points:
            self.tiles.remove(letter)
            [score, quantity] = bag.bag[letter[0]]
            quantity += 1
            bag.bag[letter[0]] = [score, quantity]

    def empty(self):
        self.tiles.clear()

    def draw_letters(self, n, bag):
        self.tiles.extend(bag.draw_tiles(n))
