import re
import os
from Game.Controller.wordtrie import Trie


ROOT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))  # project root
# TODO: fix above


class Validator(object):
    def __init__(self):
        def load_words(filename):
            words = []
            with open(filename, 'r') as file:
                for line in file:
                    words.append(line.strip())
                return words

        dictionary_dir = os.path.join(ROOT_DIR, 'Resources', 'words.txt')
        dictionary = load_words(dictionary_dir)
        self.trie = Trie(*dictionary)

    def validate_word(self, word):
        if self.trie.in_trie(word):
            return True
        else:
            return False

    @staticmethod
    def __validate_letters(player, word):
        """
        Checks if the letters in word are contained by player's rack.
        :param player:
        :param word:
        :return: True or False
        """

        rack_copy = player.tiles
        for letter in word:
            # TODO: this is going to be matched: word - aaron #player's rack - aron
            if letter not in [elem[0] for elem in rack_copy]:
                return False
            else:
                # TODO: remove works only on string and we want to remove a tuple >.>
                for (l, _) in rack_copy:
                    if l == letter:
                        rack_copy.remove((l, _))
                        break
        return True

    def validate_intersections(self, intersections):
        for word in [element[0] for element in intersections]:
            if not self.validate_word(word):
                return False
        return True

    @staticmethod
    def validate_index(coordinates):
        """
        Facilitates validation of the correctness of user input only if it's in the form '[A-O] + : + [0-14]'
        """
        if not re.match(r'^[A-O]:[1-9](?!\w)|^[A-O]:1[0-5]$', coordinates):
            return False
        return True

    @staticmethod
    def validate_initial_move(coordinates, turn, board_empty):
        """ Validates that the initial move coordinates intersect with the center
            of the grid (according to Scrabble rules). """
        if (board_empty is True and (7, 7) in coordinates) or True:
            return True
        return False

    @staticmethod
    def validate_word_intersects(board, coordinates, layout):
        """
        Checks if the "to-be written" word intersects with another one,
        already played in a previous game turn.
        """
        filled_cells = board.range_is_free(coordinates)
        if len(filled_cells) is 0 and board.is_empty():
            return True
        elif len(filled_cells) != 0:
            return True
        else:
            flag = False
            for (i, j) in coordinates:
                if board.get_cell(i, j + 1).is_empty is False or board.get_cell(i, j - 1).is_empty is False or \
                   board.get_cell(i + 1, j).is_empty is False or board.get_cell(i - 1, j).is_empty is False:
                    flag = True
                    break

            return flag

    @staticmethod
    def validate_corresponding_tiles(board, word, word_with_points, coordinates, player):
        """
        Check if every letter in the "to-be written" word has a corresponding tile on board.
        """
        existing_tiles = []  # contains the tiles, which are already on the board, prior to writing player's word

        player_rack = [element[0] for element in player.tiles]  # contains the letters only != (letter, points)
        for (index, coordinate) in enumerate(coordinates):
            if not board.get_cell(coordinate[0], coordinate[1]).holds_letter == word[index]:
                if word[index] not in player_rack:
                    # TODO: NoSuchTileInRackError(word[index], player.name)
                    return False
            else:
                existing_tiles.append(word_with_points[index])

        return existing_tiles

    @staticmethod
    def check_duplicate_word(word, game_history):
        """ Validates uniqueness of played word within the current game. """
        return not game_history.is_duplicate(word)

    @staticmethod
    def validate_unique_name(name, active_players):
        """ Validates whether the chosen player name is already in use by another player. """
        if name not in (player.name for player in active_players):
            return True
        return False

    @staticmethod
    def validate_word_length(word, coordinates):
        """ Validates that the number of letters in "word" is the same as
            the number of coordinates, given during input. """
        return len(word) == len(coordinates)

    @staticmethod
    def validate_index_correctness(start, end, layout):
        """
        Validates that the entered indexes for the starting and ending cell
        respectively are in the correct order (left to right and up to down).
        :param start: index of starting cell
        :param end: index of ending cell
        :param layout: defines the word layout; can be either 'h' (horizontal) or 'v' (vertical)
        :return:
        """
        if layout == 'h':
            # then the changing part is the column index - check if it's ascending:
            return ord(start[0]) < ord(end[0]) and int(start[1]) == int(end[1])
        else:
            # the changing part is the row index - procedure is same as above:
            return ord(start[0]) == ord(end[0]) and int(start[1]) < int(end[1])


