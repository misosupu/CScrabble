import random

from Game.Model.player import *
from Game.Model.board import *
from Game.Model.bag import *
from Game.Model.history import *
from Game.Model.boardinspector import *
from Game.Model.computer import *
from Game.Model.anagramgenerator import *
from Game.Controller.dictionary_helper import DictionaryHelper
from Game.View.statistician import *

MAX_PLAYERS = 4


class GameEngine(object):
    """ Tracks the game state. """

    def __init__(self):
        self.active_players = []
        self.turn = 0

        self.bag = Bag()
        self.board = Board()
        self.inspector = BoardInspector()
        self.history = History()
        self.dictionary_helper = DictionaryHelper()
        self.generator = AnagramGenerator()
        self.player_offset = None

    @property
    def running(self):
        """ Checks if the game is ongoing. This is true if there
            is at least one tile in the bag (i.e. - it's not empty)
            and the number of active players is more than one. """

        return (not self.bag.is_empty()) and (len(self.active_players) >= 1)

    def add_player(self, player_name, game_view):
        """ Adds a new player to the list of active players. """
        if len(self.active_players) + 1 <= MAX_PLAYERS:
            self.active_players.append(Player(player_name, self.bag))
        else:
            game_view.announce_maximum_players_exceeded()

    def remove_player(self, name):
        """
        Removes a player from the game (i.e. from the list of active players).
        :param name: name of player, who is to be removed from the game
        """
        for player in self.active_players:
            if player.name == name:
                self.active_players.remove(player)

    def add_computer_player(self):
        """ Add a computer to the game. """
        self.active_players.append(Computer(self.bag))

    def determine_first_player(self):
        """ Randomly selects an initial active player number. """
        self.player_offset = random.randint(0, len(self.active_players) - 1)

    def get_active_player(self):
        """ Returns the index (in terms of the list of active players) of the current player. """
        player_index = (self.turn + self.player_offset) % len(self.active_players)
        return self.active_players[player_index]

    def play_word(self, word, coordinates, word_with_points, player_tiles, intersections, player, game_view, validator):
        word_points = 0
        intersections_correct = True
        if isinstance(player, Computer):
            intersections_correct = validator.validate_intersections(intersections)

        if intersections_correct:
            for item in intersections:
                # intersections has the form: [item1 = (horizontal_word, start_cell, end_cell), item2, ... ]
                layout = GameEngine.__determine_word_layout([item[1], item[2]])
                word_points += self.__calculate_points(item[0], item[1], layout)

            layout = GameEngine.__determine_word_layout(coordinates)
            word_points += self.__calculate_points(word_with_points, coordinates[0], layout)

            # update model:
            player.update_points(word_points)
            self.board.place_word(coordinates, word)
            # self.board.place_tiles(start, end, word)
            self.inspector.update_options_3(self.board, sorted(list(coordinates)))

            player.remove_letters(player_tiles, self.bag)
            player.add(self.bag.draw_tiles(len(player_tiles)))
            self.turn += 1  # update turn number
            # document turn in history - document_turn(self, turn, player_name, action, word, score)
            self.history.document_turn(self.turn, player.name, "play", word, word_points)
            game_view.turn_info(player.name, word, word_points)
            # -----
            print("Inspector:\n {}".format(self.inspector))
            print("Inspector, prioritize:\n {}".format(self.inspector.prioritize_options(self.board)))
            # -----

    def change_letters(self, player, letters, game_view):
        self.turn += 1
        letters = [(letter, self.bag.get_letter_score(letter)) for letter in list(letters)]
        player.remove_letters(letters, self.bag)
        player.add(self.bag.draw_tiles(len(letters)))
        game_view.player_changed_tiles(player.name)

    def pass_turn(self, player, game_view):
        self.turn += 1
        game_view.player_passive(player.name, "passed their turn")

    def invalid_turn(self, word, player, game_view):
        self.turn += 1
        game_view.announce_invalid_word_played(word, player.name)

    def resign_game(self, player, game_view):
        self.turn += 1
        if len(self.active_players) - 1 > 1:
            game_view.player_passive(player.name, "resigned")
            self.active_players.remove(player)
        else:
            return self.__determine_winner(game_view)

    def computer_play_word(self, computer, validator, game_view):
        import itertools
        # step 1: get list of all possible permutations:
        string_permutations = self.generator.create_permutations([letter for (letter, score) in computer.tiles])
        # step 2: obtain sorted by priority list of slots
        prioritized_slots = self.inspector.prioritize_options(self.board)
        if self.board.is_empty():
            # then the Computer will make the first move, so write a random word:
            words = list(itertools.chain.from_iterable(string_permutations.values()))
            word = list(filter(validator.validate_word, words))
            if not word:
                # if the computer didn't find a place to write a word, pass turn:
                if computer.can_pass:
                    self.pass_turn(computer, game_view)
                    computer.consecutive_passes += 1  # increment consecutive passes for future reference
                else:
                    to_change = ''.join(str(e) for e in computer.tiles[:random.randint(1, 7)])
                    self.change_letters(computer, to_change, game_view)

            w = word[random.randint(0, len(word))]
            word_with_points = [(letter, self.bag.get_letter_score(letter)) for letter in w]
            coordinates = [(7, j) for j in range(6, 6 + len(word_with_points))]
            points = self.__calculate_points(word_with_points, (7, 6), 'h')
            computer.update_points(points)
            self.board.place_word(coordinates, w)
            computer.consecutive_passes = 0
            self.turn += 1
            self.history.document_turn(self.turn, computer.name, "play", ''.join(w), points)
            game_view.turn_info(computer.name, ''.join(w), points)
        else:
            found_word = False
            result = None
            while found_word is False:
                for slot in prioritized_slots:
                    result = self.inspector.test_slot(slot[1], self.board, string_permutations, validator)
                    if result is not None:
                        found_word = True
                        break
            if found_word:
                word_with_points = [(letter, self.bag.get_letter_score(letter))
                                    for letter in result[0]]
                computer_tiles = [word_with_points[i] for i in set(range(len(result[0]))).difference(set(result[2]))]
                self.play_word(result[0], result[1], word_with_points, computer_tiles, result[3],
                               computer, game_view, validator)
            else:
                if computer.can_pass():
                    self.pass_turn(computer, game_view)
                    computer.consecutive_passes += 1
                else:
                    to_change = ''.join(str(e) for e in computer.tiles[:random.randint(1, 7)])
                    self.change_letters(computer, to_change, game_view)

    def board_is_empty(self):
        return self.board.is_empty()

    def word_lookup(self, word):
        print(self.dictionary_helper.get_definition(word))

    @staticmethod
    def __get_coordinates_range(start, end):
        coordinates_range = []
        if start[0] == end[0]:  # eg - vertically
            layout = 'v'
            for j in range(int(start[1]) - 1, int(end[1])):
                coordinates_range.append((j, (ord(start[0]) - 65)))
        else:  # eg - horizontally
            layout = 'h'
            for i in range(ord(start[0]) - 65, ord(end[0]) - 64):
                coordinates_range.append((int(start[1]) - 1, i))

        return layout, coordinates_range

    def __calculate_points(self, word, start_index, layout):
        triple_word_flag = False
        double_word_flag = False
        points = 0
        if type(word) is str:  # i.e. - if the word came from an intersection
            word = [(letter, self.bag.bag[letter][0]) for letter in word]
        if layout == 'h':  # e.g. - horizontally
            i = start_index[1]
            for (letter, score) in word:
                points += score * self.board.get_cell(start_index[0], i).multiply_by
                if self.board.get_cell(start_index[0], i).type is "TW" and \
                        self.board.get_cell(start_index[0], i).is_empty:
                    triple_word_flag = True
                elif self.board.get_cell(start_index[0], i).type is "DW" and \
                        self.board.get_cell(start_index[0], i).is_empty:
                    double_word_flag = True
                i += 1

        else:  # e.g. -  vertically
            i = start_index[0]
            for (letter, score) in word:
                points += score * self.board.get_cell(i, start_index[1]).multiply_by
                if self.board.get_cell(i, start_index[1]).type is "TW" and \
                        self.board.get_cell(i, start_index[1]).is_empty:
                    triple_word_flag = True
                elif self.board.get_cell(i, start_index[1]).type is "DW" and \
                        self.board.get_cell(i, start_index[1]).is_empty:
                    double_word_flag = True
                i += 1

        if triple_word_flag is True:
            return points * 3
        elif double_word_flag is True:
            return points * 2
        else:
            return points

    @staticmethod
    def __determine_word_layout(coordinates):
        if coordinates[0][0] == coordinates[1][0]:
            layout = 'h'
        else:
            layout = 'v'
        return layout

    def __determine_winner(self, game_view):
        winner = sorted(self.active_players, key=lambda player: player.points, reverse=True)[0]
        game_view.announce_winner(winner)
        self.active_players.clear()
        s = Statistician()
        s.output_statistics(self.history, [player.name for player in self.active_players])
