from Game.Model.computer import Computer
from Game.View.consoleview import *
from Game.Validators.validator import *


class ConsoleController(object):
    """ Handles user input. """

    def __init__(self, game_engine):
        self.view = ConsoleView()
        self.engine = game_engine
        self.validator = Validator()
        self.action_functions = [self.play_word_input, self.change_tiles_input,
                                 self.pass_turn, self.resign, self.get_help]
        self.initialize_players()

    def initialize_players(self):
        available_choices = ['Y', 'N']
        flag = True
        while flag:
            self.add_player()
            desire = input("Would you like to add another player?\nY/N? ").capitalize()
            while desire not in available_choices:
                print("Invalid choice. Try again.")
                desire = input("Add another player?\nY/N?").capitalize()
            if desire == 'N':
                if len(self.engine.active_players) < 2:
                    print("Error: Cannot play a game with only one player.\n")
                    desire = input("Would you like to add a computer as an opponent?\nY/N? ").capitalize()
                    if desire == 'Y':
                        self.engine.add_computer_player()
                        flag = False  # new players will not be added to the game
                else:
                    flag = False  # new players will not be added to the game
        self.engine.determine_first_player()

    def add_player(self):
        name = input("Enter new player name: ")
        while self.validator.validate_unique_name(name, self.engine.active_players) is False:
            name = input("Name already exists, enter new player name: ")
        self.engine.add_player(name, self.view)

    def run(self, game_view):
        while self.engine.running:
            active_player = self.engine.get_active_player()
            self.view.show_board(self.engine.board)
            self.view.announce_turn(active_player)
            self.view.rack_info(active_player)
            # if the active player is a computer - computer -> play word:
            if isinstance(active_player, Computer):
                self.engine.computer_play_word(active_player, self.validator, game_view)
            else:
                action_taken = int(self.choose_action(game_view)) - 1
                self.action_functions[action_taken](active_player, game_view)

    def play_word_input(self, player, game_view):
        word = input("Word: ")

        while word.isalpha() is False:
            word = input("You didn't enter a valid word, try again: ")

        if self.validator.validate_word(word) is True:
            word_with_points = [(letter, self.engine.bag.get_letter_score(letter)) for letter in word]
            start, end = self.get_coordinates_input(self.validator)

            start = start.split(':')
            end = end.split(':')

            layout, coordinates = ConsoleController.__get_coordinates_range(start, end)
            if self.validator.validate_word_length(word, coordinates) and \
                    self.validator.validate_index_correctness(start, end, layout):
                full_coordinates = coordinates  # keeps list of the full coordinates list (for intersections check)

                filled_cells = self.engine.board.range_is_free(coordinates)

                if self.validator.validate_initial_move(coordinates, self.engine.turn,
                                                        self.engine.board.is_empty()) is True:
                    if self.validator.validate_word_intersects(self.engine.board, coordinates, layout):
                        # TODO: fox validate_corresponding tiles
                        existing_tiles = self.validator.validate_corresponding_tiles(self.engine.board, word,
                                                                                     word_with_points,
                                                                                     full_coordinates, player)
                        # validator.validate_corresponding_tiles is True:

                        if self.validator.check_duplicate_word(word, self.engine.history) is True:
                            coordinates = set(coordinates).difference(filled_cells)
                            if layout == 'v':
                                intersections = self.engine.board.get_intersecting_words('v', full_coordinates,
                                                                                         coordinates, word)
                            else:  # eg. layout == 'h'
                                intersections = self.engine.board.get_intersecting_words('h', full_coordinates,
                                                                                         coordinates, word)
                            if self.validator.validate_intersections(intersections) is True:
                                player_tiles = [element for element in word_with_points
                                                if element not in existing_tiles]
                                self.engine.play_word(word, full_coordinates, word_with_points, player_tiles,
                                                      intersections, player, game_view, self.validator)
            else:
                print("The input was incorrect\nPlease make sure that the entered coordinates "
                      "range matches the word length and that they are given in the correct order - \n"
                      "left-to-right if the word layout is horizontal and\n"
                      "up-to-down if the word layout is vertical.")
                self.play_word_input(player, game_view)
        else:
            self.engine.invalid_turn(word, player, game_view)

    def change_tiles_input(self, player, game_view):
        letters = self.get_input_letters()
        self.engine.change_letters(player, letters, game_view)

    def get_coordinates_input(self, validator):
        start = self.get_index_input(validator, "start")
        end = self.get_index_input(validator, "end")
        return start, end

    def pass_turn(self, player, game_view):
        self.engine.pass_turn(player, game_view)

    def resign(self, player, game_view):
        self.engine.resign_game(player, game_view)

    def get_help(self, player, game_view):
        """ Get help about definition of a word, already played on board (no cheating!). """
        word = input("Which word would you like defined?")
        retries = 3
        while not self.engine.history.is_duplicate(word) and retries != 0:
            word = input("The word must be an already existing one on board. "
                         "Try again (number of retires left: {})".format(retries))
            retries -= 1
        self.engine.word_lookup(word)

    @staticmethod
    def choose_action(console_view):
        console_view.list_actions()
        action = input("Enter action number: ")
        while action not in ['1', '2', '3', '4', '5']:
            action = input("Enter action number: ")
        return action

    @staticmethod
    def get_input_letters():
        """
        Handle input for which letters are to be changed from player's rack.
        """
        letters = input("Choose letters to remove: ")
        while letters.isalpha() is False:
            print(ConsoleController.__error_message('letters'))

        return letters

    @staticmethod
    def __error_message(error_type):
        return "Invalid {} input. Try again".format(error_type)

    @staticmethod
    def __get_coordinates_range(start, end):
        """
        Get range of coordinates for the word which is to be written.
        :param start: Coordinates (i, j) of the first tile cell.
        :param end: Coordinates (i, j) of the last tile cell.
        :return: layout (horizontal or vertical) of the word and the coordinates range.
        """
        coordinates_range = []
        if start[0] == end[0]:  # eg - vertically
            layout = 'v'
            for j in range(int(start[1]) - 1, int(end[1])):
                coordinates_range.append((j, (ord(start[0]) - 65)))
        else:  # eg - horizontally
            layout = 'h'
            for i in range(ord(start[0]) - 65, ord(end[0]) - 64):
                coordinates_range.append((int(start[1]) - 1, i))
        return layout, coordinates_range

    @staticmethod
    def get_index_input(validator, place):
        """
        Get input of board cell index (for start or end of the word, respectively).
        :param validator: To validate the correctness of the input.
        :param place: Contains information whether it is the starting or ending word cell.
        :return: index
        """
        index = input("Enter {} tile: ".format(place))
        while validator.validate_index(index) is False:
            print(ConsoleController.__error_message('index'))
            index = input("Enter {} tile: ".format(place))
        else:
            return index
