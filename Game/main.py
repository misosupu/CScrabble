from gameengine import *
from Game.Controller.consolecontroller import *


def run():
    controller = ConsoleController(GameEngine())
    view = ConsoleView()
    controller.run(view)

if __name__ == '__main__':
    run()
